
#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <gio/gio.h>
#include <gio/gunixinputstream.h>
#include <gio/gunixoutputstream.h>
#include <libsoup/soup.h>

#define BUFFER 1024

typedef struct
{
  GPid          child_pid;
  GInputStream  *local_input;
  GInputStream  *input;
  GInputStream  *error;
  GOutputStream *output;

  void          *error_buffer;
  void          *input_buffer;
  void          *local_input_buffer;

  GString       *buffer;
  gint64         position;
  gint           max_length;

  GMainLoop     *main_loop;
  SoupServer    *server;
} APData;

static void
read_async (GInputStream        *stream,
            void                *buffer,
            gint                 buffer_len,
            GAsyncReadyCallback  callback,
            gpointer             userdata)
{
  g_input_stream_read_async (stream,
                             buffer,
                             buffer_len,
                             G_PRIORITY_DEFAULT,
                             NULL,
                             callback,
                             userdata);
}

static void
truncate_string (APData *data)
{
  while (data->buffer->len >= data->max_length)
    {
      gchar *newline = g_utf8_strchr (data->buffer->str, -1, '\n');

      if (!newline)
        break;

      g_string_erase (data->buffer, 0, (newline - data->buffer->str) + 1);
    }
}

static void
append_string (APData      *data,
               gint         length,
               const gchar *string)
{
  if (length)
    {
      g_string_append_len (data->buffer, string, length);
      data->position += length;
      truncate_string (data);

      g_print ("%.*s", length, string);
    }
}

static void
input_callback (GObject      *source_object,
                GAsyncResult *result,
                gpointer      userdata)
{
  gssize bytes_read;

  GError *error = NULL;
  APData *data = userdata;

  bytes_read = g_input_stream_read_finish (data->input,
                                           result,
                                           &error);

  if (bytes_read == -1)
    {
      g_warning ("Error reading from child process: %s", error->message);
      g_error_free (error);
      g_main_loop_quit (data->main_loop);
      return;
    }

  append_string (data, bytes_read, (const gchar *)data->input_buffer);

  read_async (data->input, data->input_buffer, BUFFER, input_callback, data);
}

static void
error_input_callback (GObject      *source_object,
                      GAsyncResult *result,
                      gpointer      userdata)
{
  gssize bytes_read;

  GError *error = NULL;
  APData *data = userdata;

  bytes_read = g_input_stream_read_finish (data->error,
                                           result,
                                           &error);

  if (bytes_read == -1)
    {
      g_warning ("Error reading from child process: %s", error->message);
      g_error_free (error);
      g_main_loop_quit (data->main_loop);
      return;
    }

  append_string (data, bytes_read, (const gchar *)data->error_buffer);

  read_async (data->error, data->error_buffer,
              BUFFER, error_input_callback, data);
}

static void
local_input_callback (GObject      *source_object,
                      GAsyncResult *result,
                      gpointer      userdata)
{
  gssize bytes_read;

  GError *error = NULL;
  APData *data = userdata;

  bytes_read = g_input_stream_read_finish (data->local_input,
                                           result,
                                           &error);

  if (bytes_read == -1)
    {
      g_warning ("Error reading from standard input: %s", error->message);
      g_error_free (error);
      g_main_loop_quit (data->main_loop);
      return;
    }

  if (bytes_read &&
      !g_output_stream_write_all (data->output,
                                  data->local_input_buffer,
                                  bytes_read,
                                  NULL,
                                  NULL,
                                  &error))
    {
      g_warning ("Error writing to child input: %s", error->message);
      g_error_free (error);
      g_main_loop_quit (data->main_loop);
      return;
    }

  read_async (data->local_input, data->local_input_buffer,
              BUFFER, local_input_callback, data);
}

static void
handle_output (APData      *data,
               SoupMessage *message,
               SoupURI     *uri)
{
  gint start;
  gchar *reply;

  if (uri->query)
    start = CLAMP (data->buffer->len - (data->position - atoi (uri->query)),
                   0, data->buffer->len);
  else
    start = 0;

  reply =
    g_strdup_printf ("%" G_GINT64_FORMAT "\n%s",
                     data->position,
                     data->buffer->str + start);

  soup_message_set_status (message, 200);
  soup_message_set_response (message,
                             "text/plain",
                             SOUP_MEMORY_TAKE,
                             reply,
                             strlen (reply));
}

static void
handle_input (APData          *data,
              SoupMessage     *message,
              SoupMessageBody *body)
{
  GError *error = NULL;

  if (body->length &&
      !g_output_stream_write_all (data->output,
                                  body->data,
                                  body->length,
                                  NULL,
                                  NULL,
                                  &error))
    {
      g_warning ("Error writing to child input: %s", error->message);
      g_error_free (error);
      g_main_loop_quit (data->main_loop);
      return;
    }

  soup_message_set_status (message, 200);
  soup_message_set_response (message,
                             "text/plain",
                             SOUP_MEMORY_STATIC,
                             NULL,
                             0);
}

static void
request_read_callback (SoupServer        *server,
                       SoupMessage       *message,
                       SoupClientContext *client,
                       gpointer           userdata)
{
  gchar *method;
  SoupMessageBody *body;

  APData *data = userdata;
  SoupURI *uri = soup_message_get_uri (message);

  g_object_get (G_OBJECT (message),
                "method", &method,
                "request-body", &body,
                NULL);

  /*g_debug ("Request-read:\n"
           "\tScheme: %s\n"
           "\tUser: %s\n"
           "\tPassword: %s\n"
           "\tHost: %s\n"
           "\tPort: %u\n"
           "\tPath: %s\n"
           "\tQuery: %s\n"
           "\tFragment: %s\n"
           "\tMethod: %s\n"
           "\tBody: %.*s",
           uri->scheme,
           uri->user,
           uri->password,
           uri->host,
           uri->port,
           uri->path,
           uri->query,
           uri->fragment,
           method,
           (gint)body->length,
           body->data);*/

  if (uri->path && method)
    {
      if (g_str_equal (uri->path, "/output/") &&
          g_str_equal (method, "GET"))
        handle_output (data, message, uri);
      else if (g_str_equal (uri->path, "/input") &&
               g_str_equal (method, "POST"))
        handle_input (data, message, body);
    }

  g_free (method);
  soup_message_body_free (body);
}

int
main (int argc, char **argv)
{
  gint i;
  gint child_argc;
  gchar **child_argv;
  gint input_fd, output_fd, error_fd;

  gint port = 8080;
  APData data = { 0, };

  GError *error = NULL;

  data.max_length = 80*24*4;

  /* Parse arguments */
  for (i = 0; i < argc; i++)
    {
      if (g_str_equal (argv[i], "-p"))
        {
          if (++i == argc)
            break;

          port = atoi (argv[i]);
        }
      else if (g_str_equal (argv[i], "-l"))
        {
          if (++i == argc)
            break;

          data.max_length = MAX (80, atoi (argv[i]));
        }
      else if (g_str_equal (argv[i], "--"))
        {
          i++;
          break;
        }
    }

  child_argc = argc - i;
  if (!child_argc)
    {
      g_message ("Syntax:\n"
                 "\tajax-pipes [-p port] [-l buffer_size] -- <command-line>");
      return 1;
    }

  /* Load child */
  child_argv = g_new0 (gchar *, argc);
  for (; i < argc; i++)
    child_argv[i - (argc - child_argc)] = argv[i];

  if (!g_spawn_async_with_pipes (NULL,
                                 child_argv,
                                 NULL,
                                 G_SPAWN_SEARCH_PATH,
                                 NULL,
                                 NULL,
                                 &data.child_pid,
                                 &input_fd,
                                 &output_fd,
                                 &error_fd,
                                 &error))
    {
      g_warning ("Error spawning child: %s", error->message);
      g_error_free (error);
      g_free (child_argv);
      return 1;
    }

  g_free (child_argv);

  /* Initialise type system */
  g_type_init ();

  /* Create streams/allocate data */
  data.local_input = g_unix_input_stream_new (STDIN_FILENO, FALSE);
  data.input = g_unix_input_stream_new (output_fd, TRUE);
  data.error = g_unix_input_stream_new (error_fd, TRUE);
  data.output = g_unix_output_stream_new (input_fd, TRUE);
  data.input_buffer = g_malloc (BUFFER);
  data.error_buffer = g_malloc (BUFFER);
  data.local_input_buffer = g_malloc (BUFFER);
  data.buffer = g_string_new (NULL);
  data.main_loop = g_main_loop_new (g_main_context_default (), FALSE);

  /* Start server */
  g_message ("Running server on port %d", port);
  data.server = soup_server_new ("port", port,
                                 "server-header", "AjaxPipes/0.0 ",
                                 NULL);
  g_signal_connect (data.server, "request-read",
                    G_CALLBACK (request_read_callback), &data);
  soup_server_run_async (data.server);

  /* Run main-loop */
  read_async (data.input, data.input_buffer, BUFFER, input_callback, &data);
  read_async (data.error, data.error_buffer,
              BUFFER, error_input_callback, &data);
  read_async (data.local_input, data.local_input_buffer,
              BUFFER, local_input_callback, &data);
  g_main_loop_run (data.main_loop);

  /* Free data */
  soup_server_quit (data.server);
  g_object_unref (data.server);

  g_main_loop_unref (data.main_loop);

  g_spawn_close_pid (data.child_pid);

  g_input_stream_close (data.local_input, NULL, NULL);
  g_object_unref (data.local_input);

  g_input_stream_close (data.input, NULL, NULL);
  g_object_unref (data.input);

  g_input_stream_close (data.error, NULL, NULL);
  g_object_unref (data.error);

  g_output_stream_close (data.output, NULL, NULL);
  g_object_unref (data.output);

  g_free (data.input_buffer);
  g_free (data.error_buffer);
  g_free (data.local_input_buffer);
  g_string_free (data.buffer, TRUE);

  return 0;
}

