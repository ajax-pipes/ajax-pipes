
ajax-pipes: ajax-pipes.c
	gcc -o ajax-pipes ajax-pipes.c -Wall -g -O0 `pkg-config --cflags --libs glib-2.0 gio-unix-2.0 libsoup-2.4`

clean:
	rm -f ajax-pipes
